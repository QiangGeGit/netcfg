// netcfg.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>

#include <Netcfgx.h>

class CNetCfg {
public:
	CNetCfg(LPCWSTR clientName) {
		do 
		{
			m_hrLast = CoCreateInstance(CLSID_CNetCfg, NULL, CLSCTX_SERVER,
				IID_INetCfg, (LPVOID*)&m_pnetcfg);
			if (m_hrLast != S_OK) {
				break;
			}
			m_hrLast = m_pnetcfg->QueryInterface(IID_INetCfgLock, (LPVOID*)&m_pncfglock);
			if (m_hrLast != S_OK) {
				break;
			}
			m_hrLast = m_pncfglock->AcquireWriteLock(5000, clientName, NULL);
			if (m_hrLast != S_OK) {
				break;
			}
			m_hrLast = m_pnetcfg->Initialize(NULL);
			if (m_hrLast != S_OK) {
				break;
			}
			return;
		} while (false);
		Release();
	}
	~CNetCfg() {
		Release();
	}

	operator bool() {
		return !!m_pnetcfg;
	}

	bool Apply();
	void Cancel();
private:
	void Release() {
		if (!m_pnetcfg) {
			return;
		}
		m_pnetcfg->Uninitialize();
		m_pncfglock->ReleaseWriteLock();
		m_pncfglock->Release();
		m_pnetcfg->Release();
		m_pnetcfg = NULL;
	}

private:
	INetCfg* m_pnetcfg = NULL;
	INetCfgLock* m_pncfglock = NULL;

	HRESULT m_hrLast=S_OK;
};

int main()
{
    std::cout << "Hello World!\n";

	INetCfg* pnetcfg = NULL;
	INetCfgLock* pncfglock = NULL;
	INetCfgComponent* pncfgcomp = NULL;
	HRESULT hr = S_OK;
	LPWSTR szwrClient = NULL;

	hr = CoInitialize(NULL);

	hr = CoCreateInstance(CLSID_CNetCfg, NULL, CLSCTX_SERVER,
		IID_INetCfg, (LPVOID*)&pnetcfg);
	hr = pnetcfg->QueryInterface(IID_INetCfgLock, (LPVOID*)&pncfglock);
	hr = pncfglock->AcquireWriteLock(5000, L"MY CLIENT", &szwrClient);
	// hr is S_FALSE if the lock could not be acquired in 5 seconds
	hr = pnetcfg->Initialize(NULL);

	hr = pnetcfg->FindComponent(L"MS_TCPIP", &pncfgcomp);
	// Call various methods on pncfgcomp here.


	pncfgcomp->Release();

	hr = pnetcfg->Apply();
	hr = pnetcfg->Uninitialize();
	hr = pncfglock->ReleaseWriteLock();
	pnetcfg->Release();

	CoUninitialize();
}

// Run program: Ctrl + F5 or Debug > Start Without Debugging menu
// Debug program: F5 or Debug > Start Debugging menu

// Tips for Getting Started: 
//   1. Use the Solution Explorer window to add/manage files
//   2. Use the Team Explorer window to connect to source control
//   3. Use the Output window to see build output and other messages
//   4. Use the Error List window to view errors
//   5. Go to Project > Add New Item to create new code files, or Project > Add Existing Item to add existing code files to the project
//   6. In the future, to open this project again, go to File > Open > Project and select the .sln file
